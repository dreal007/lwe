(function(){

    angular.module('app', ['ngStorage'])
    // .value('baseUrl', 'http://localhost/lwe')
    .value('baseUrl', 'https://loveworldexhibition.online/virtual')
    .controller('HomeCntrl', ['$interval','$rootScope','$scope', '$http','$localStorage','$timeout' ,'baseUrl','$httpParamSerializer', '$window' , homeCntrl]);

  function homeCntrl ($interval, $rootScope, $scope, $http, $localStorage ,$timeout, baseUrl, $httpParamSerializer, $window ){
      var vm = this;

      vm.user_token = $localStorage.user_access_token ? btoa($localStorage.user_access_token) : ''
      vm.user_id = $localStorage.user_id ? $localStorage.user_id : ''
      

      //**********************************Get country list******************************* */
      var params = {
          url : baseUrl + '/assets/countries.json',
          method: 'GET',
          headers: {'Content-Type' : 'application/json'}
      };
      $http(params).then( res =>{
          vm.countries = res.data
        //   console.log(vm.countries)
      }).catch( err =>{ console.log(err)})


    //   vm.$doCheck = function(){
    //     if((vm.user_token !== btoa($localStorage.user_access_token)) || (vm.user_id !== $localStorage.user_id)){
    //        vm.user_token = btoa($localStorage.user_access_token);
    //        vm.user_id = $localStorage.user_id;
    //        console.log('user token updated'); 
    //     }
    // }
    
    //*************************************Date Picker******************************************** */
    vm.toggleDate = function(id, obj){
        var obj = obj;
        $(id).datetimepicker({
            widgetPositioning: {
                horizontal: 'left',
                vertical: 'bottom'
            },
            viewMode : 'years',
            format : 'YYYY-MM-DD'
        })
        obj.birthday = $(id).val();
        console.log(obj)

     }

   //*******************************************Close Video******************************* */  
    $("#promo").on('hidden.bs.modal', function (e) {
        //$("#promo video").attr("src", '');
        $("#promo video").trigger('pause');
    });



   //***************************************Activate Chat**************************************/
    vm.activateChat = function(){
        var Tawk_API=Tawk_API||{};
        var Tawk_LoadStart = new Date();
      (function(){
      var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
      s1.async=true;
      s1.src='https://embed.tawk.to/5bf5496279ed6453ccaa6ae6/default';
      s1.charset='UTF-8';
      s1.setAttribute('crossorigin','*');
      s0.parentNode.insertBefore(s1,s0);
      })();
    }

  //********************************************Single Modal for Multiple videos*************/  
    vm.showVideoModal = function(videoId){
        vm.videoId = videoId
        $("#promo video").attr('src', baseUrl+'/assets/videos/'+vm.videoId+'.mp4');
        $('#promo').modal('show');

    }

    vm.showModal = function (modalId, vidUrl){
        $('#' + modalId + ' video').attr('src', baseUrl + '/assets/videos/' + vidUrl + '.mp4');
        $('#' + modalId).modal('show');
    }
  //*************************************************Enter***********************************/

    vm.enter = function(phone){
        var params = {
            url : baseUrl+'/home/enter',
            method : 'POST',
            transformRequest: angular.identity,
            headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
            data : $.param({phone : phone})
        }
        $http(params).then((res)=>{
            if(res.data.status === true){
                console.log(res.data.data[0]);
                $localStorage.user_access_token = res.data.data[0].access_token;
                $localStorage.user_id = res.data.data[0].id;
                $("#enter").modal('hide');
                $.notify({ title: "<h5>Registration</h5>", message: "Your have succesfully Logged In!"}, { type: "info"});
                $timeout(2000).then(()=>{
                    $window.location.href = baseUrl+'/home/reception?q='+vm.user_token;
                });
            }
            else{
                console.log(res);
                $("#enter").modal('hide');
                $.notify({ title: "<h5>Registration</h5>", message: "Phone number does not exist, Try again!"}, { type: "warning"});
            }
        })
        .catch((err)=>{
            console.log(err);
            $("#enter").modal('hide');
            $.notify({ title: "<h5>Registration</h5>", message: "An error occured on the server, Try again!"}, { type: "danger"});
        });
    }

    //******************************************Notification********************************************
    vm.notiffy = function(msg){
      let cnt = 0;
      let len = msg.length;
        $interval(function(){
            if(cnt < len){
                $.notify({ title : "<h5>Information tips</h5>", message: msg[cnt]}, {type: "primary", delay:8000})
                cnt++;
            }
        }, 20000)
    }

    // ****************************************Signup****************************************************
    vm.signup = function(ministry_arm){
        let signup = {
          ministry_arm : ministry_arm,
          user_id : vm.user_id
        }
        var params = {
            url : baseUrl+'/home/signup',
            method : 'POST',
            transformRequest: angular.identity,
            headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
            data : $.param({signup : signup})
        }
        $http(params).then((res)=>{
            if(res.data.status === true){
                console.log(res.data);
                $.notify({ title: "<h5>Sign Up</h5>", message: "Your have succesfully Signed Up!"}, { type: "success"});
            }
            else{
                console.log(res);
                $.notify({ title: "<h5>Signup</h5>", message: "Signup not successful, Try again!"}, { type: "warning"});
            }
        })
        .catch((err)=>{
            console.log(err);
            $("#enter").modal('hide');
            $.notify({ title: "<h5>Registration</h5>", message: "An error occured on the server, Try again!"}, { type: "danger"});
        });
    }

    //*************************************** Exit*******************************************************

    vm.exit = function(){
        $("#exit").modal('show');
        $timeout(4000).then(()=>{
            $("#exit").modal('hide');
            $.notify({ title: "<h5>Welcome</h5>", message: "Successful"}, { type: "success"});
            $window.location.href = "https://pastorchrisonline.org";
        });
    }

    //*************************************Register*****************************************************

    vm.register = function(user){
        console.log(user);
        var params = {
            url : baseUrl+'/home/reg',
            method : 'POST',
            transformRequest: angular.identity,
            headers : {'Content-Type' : 'application/x-www-form-urlencoded'},
            data : $.param({user : user})
        }
        $http(params).then((res)=>{
            if(res.data.status === true){
                $localStorage.user_access_token = res.data.access_token;
                $("#register").modal('hide');
                $.notify({ title: "<h5>Registration</h5>", message: "Your have succesfully registered!"}, { type: "info"});
                // $timeout(2500).then(()=>{
                //     $window.location.href =baseUrl+ '/home/newpage?message='+res.data.message;
                // });
            }
            else{
                console.log(res.data);
                $.notify({ title: "<h5>Registration</h5>", message: "Email or Phone number already exists, Try again!"}, { type: "warning"});
            }
        })
        .catch((err)=>{
            console.log(err);
            $.notify({ title: "<h5>Registration</h5>", message: "An error occured on the server, Try again!"}, { type: "danger"});
        });
      }

}
})();
