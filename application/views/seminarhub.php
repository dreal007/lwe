<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld Exhibition Seminar Hub</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
      <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/shubgate.png" class="img-fluid rounded mx-auto d-block" usemap="#shubgate-map">


    <map name="shubgate-map">
      <area target="" alt="Seminar hub live stream link" title="Seminar hub live stream link" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="577,529,374,470" shape="rect">
      <area target="" alt="Seminar hub past event link" title="Seminar hub past event link" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="609,467,805,532" shape="rect">
      <area target="" alt="Previous Page " title="Previous Page" href="<?php echo base_url('home/campus_a?q=').'{{vm.user_token}}';?>" coords="482,29,32" shape="circle">
      <area target="" alt="Home Page" title="Home Page" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="546,30,25" shape="circle">
      <area target="" alt="Share" title="Share" coords="620,31,30" shape="circle">
      <area target="" alt="Info" title="Info" coords="696,30,29" shape="circle">
    </map>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>
      
      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      });

    </script>

  </body>
</html>
