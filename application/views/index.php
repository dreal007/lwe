<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <!-- <base href="/lwe/"> -->
    <base href="/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld Exhibition</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" >
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
      <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/exhibition.png" class="img-fluid rounded mx-auto d-block" usemap="#home-map">

    <map name="home-map">
        <!-- <area target="_blank" alt="Enter Exhibition" title="Enter Exhibition" data-toggle="modal" data-target="#enter" coords="485,424,579,471" shape="rect"> -->
        <!-- <area target="_blank" alt="Register" title="Register" data-toggle="modal" data-target="#register" coords="663,454,775,513" shape="rect"> -->
        <area target="_self" alt="Enter Exhibition" title="Enter Exhibition" href="<?php echo base_url('home/reception');?>" coords="485,424,579,471" shape="rect">
        <area target="" alt="Exit" title="Exit" ng-click="vm.exit();" data-toggle="modal" data-target="#exit" coords="665,427,761,469" shape="rect">
    </map>


    <!--Enter Modal -->
      <div class="modal fade" id="enter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" style="margin: 0 auto;">Enter The Loveworld Exhibition</h5>
                </div>
                <form name="enterform" ng-submit="vm.enter(phone_id);">
                  <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-10 offset-md-1">
                          <label for="phone">Enter Kingschat Number</label>
                          <input type="text" class="form-control" name="phone" id="phone" ng-model="phone_id"  placeholder="+2348076543469" ng-required="true">
                          <small ng-show="enterform.phone.$error.required && enterform.phone.$dirty" class="form-text text-info"> Please provide a registered phone number</small>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" ng-disabled="enterform.$invalid" class="btn btn-primary">Enter</button>
                  </div>
                </form>
              </div>

          </div>
        </div>

        <!--Exit Modal -->
      <div class="modal fade" id="exit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" style="margin: 0 auto;">Exit The Loveworld Exhibition</h5>
                </div>
                <form name="enterform" ng-submit="vm.logout();">
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" ng-disabled="enterform.$invalid" class="btn btn-primary">Exit</button>
                  </div>
                </form>
              </div>

          </div>
        </div>

        <!--Register Modal -->
      <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" style="margin: 0 auto;">Register For The Loveworld Exhibition</h5>
                </div>
                <form name="regform">
                  <div class="modal-body">
                    <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="fullname">Full Name</label>
                          <input type="text" class="form-control" name="fullname" ng-model="user.fullname" placeholder="E.g Adams Smith" ng-required="true">
                      </div>
                      <div class="form-group col-md-6">
                          <label for="email">Email</label>
                          <input type="email" class="form-control" name="email" ng-model="user.email" placeholder="smith@gmail.com" ng-required="true">
                      </div>
                      <div class="form-group col-md-6">
                          <label for="phone">Kingschat Number</label>
                          <input type="text" class="form-control" name="phone" ng-model="user.phone" ng-required="true">
                      </div>
                      <div class="form-group col-md-6">
                          <label for="phone">Gender</label>
                          <select id="inputState" class="form-control" name="gender" ng-model="user.gender"  ng-required="true">
                              <!-- <option selected disabled>Choose gender</option> -->
                              <option value="male">Male</option>
                              <option value="female">Female</option>
                          </select>
                      </div>
                      <div class="form-group col-md-6" ng-init="user.birthday=''">
                          <label for="phone">Birthday</label>
                          <input class="form-control" ng-click="vm.toggleDate('#dob', user)" ng-mouseover="vm.toggleDate('#dob', user)" id="dob" name="dob" placeholder="Birthday" ng-model="user.birthday" value="{{user.birthday}}" ng-required="true">
                      </div>
                      <div class="form-group col-md-6">
                          <label for="phone">Church</label>
                          <input type="text" class="form-control" name="church" ng-model="user.church" ng-required="true">
                          <small class="form-text text-muted">E.g C.E Texas</small>
                      </div>
                      <div class="form-group col-md-6">
                          <label for="phone">Designation</label>
                          <select class="form-control" name="designation" ng-model="user.designation" ng-required="true">
                            <!-- <option ng-selected="true" disabled >Choose One</option> -->
                            <option value="Pastor">Pastor</option>
                            <option value="Asst Pastor">Asst Pastor</option>
                            <option value="Coordinator">Coordinator</option>
                            <option value="Deacon">Deacon</option>
                            <option value="Deaconess">Deaconess</option>
                            <option value="Evangelist">Evangelist</option>
                            <option value="Reverend">Reverend</option>
                            <option value="Brother">Brother</option>
                            <option value="Sister">Sister</option>

                          </select>
                      </div>
                      <div class="form-group col-md-6">
                          <label for="phone">Country</label>
                          <select class="form-control" name="country" ng-model="user.country"  ng-required="true">
                             <option ng-repeat="country in vm.countries" value="{{country.name}}">{{country.name}} ({{country.code}})</option>
                          </select>
                      </div>
                      <div class="form-group col-md-6">
                          <label for="phone">Zone</label>
                          <input type="text" class="form-control" name="zone" ng-model="user.zone" ng-required="true">
                          <small class="form-text text-muted">E.g UK Zone 2</small>
                      </div>
                      <div class="form-group col-md-6">
                          <label for="phone">Region</label>
                          <input type="text" class="form-control" name="region" ng-model="user.region" ng-required="true">
                          <small class="form-text text-muted">E.g UK Zone Region</small>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button ng-disabled="regform.$invalid" type="submit" class="btn btn-primary" ng-click="vm.register(user);">Register</button>
                  </div>
                </form>
              </div>

          </div>
        </div>
  </div>


    <!-- <footer class="container">
      <p>&copy; Company 2017-2018</p>
    </footer> -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>

      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      });

    </script>

  </body>
</html>
