<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld SAT</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/ionicons.min.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
    <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/lwsat.png" class="img-fluid rounded mx-auto d-block" usemap="#campc-map">
      <map name="campc-map">
        <area target="" alt="Previous Page" title="Previous Page" href="<?php echo base_url('home/campus_c?q=').'{{vm.user_token}}';?>" coords="490,28,25" shape="circle">
        <area target="" alt="Home" title="Home" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="551,31,25" shape="circle">
        <area target="" alt="Share Link" title="Share Link" href="#" coords="622,27,26" shape="circle">
        <area target="" alt="Info" title="Info" coords="696,31,26" shape="circle">
        <area target="" alt="Click to Watch Promo" title="Click to Watch Promo" href="promo" data-toggle="modal" data-target="#promo" coords="504,332,686,440" shape="rect">
        <area target="" alt="Chat" title="Chat" ng-click="vm.activateChat()" coords="243,671,409,722" shape="rect">
        <area target="" alt="Contact" title="Contact" coords="418,671,596,721" data-toggle="modal" data-target="#contact" shape="rect">
        <area target="" alt="Sign Up For Loveworld SAT" title="Sign Up For Loveworld SAT " ng-click="vm.signup('loveworld_sat')" coords="607,671,786,722" shape="rect">
        <area target="" alt="Downloads" title="Downloads" href="<?php echo base_url('home/download/loveworld_sat');?>" coords="793,672,959,722" shape="rect">
        <area target="" alt="Brief Overview" title="Brief Overview" data-target="#overview" data-toggle="modal" coords="400,116,691,226" shape="rect">
        <area target="" alt="Brief Overview" title="Brief Overview" data-target="#overview" data-toggle="modal" coords="631,518,797,623" shape="rect">
        <area target="" alt="Receptionist" title="Receptionist" coords="726,386,673,514,791,516" shape="poly">
      </map>

      <!--Promo Modal -->
      <div class="modal fade" id="promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <!-- <div class="modal-header">
                  <h5 class="modal-title" style="margin: 0 auto;">Watch Promo</h5>
                </div> -->
                  <div class="modal-body">
                  <div class="embed-responsive embed-responsive-16by9">
                      <video  controls id="innercity" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
                        <source src="<?php echo base_url();?>assets/videos/lwsat.mp4" type="video/mp4">
                      </video>
                  </div>
                  </div>
                  <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" ng-disabled="enterform.$invalid" class="btn btn-primary">Exit</button>
                  </div> -->
              </div>

          </div>
      </div>

      <!--Overview Modal -->
      <div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">Contact Us @ Loveworld SAT</h5>
                </div>
                  <div class="modal-body">
                    <p style="font-size:18px;"></p>
                      <strong>Address</strong> : 333 Pretoria Avenue, Randburg, Gauteng, South-Africa, 2194  <br>
                      <strong>Phone  </strong> : +27 11 781 8340-4, +27 86 156 8396 <br>
                      <strong>Email  </strong> : feedbacks@loveworldsat.org  <br>
                      <strong>Website</strong> : http://www.loveworldsat.org/watchlisten/live-tv 

                    </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>

       <!--Overview Modal -->
       <div class="modal fade" id="overview" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">Loveworld SAT</h5>
                </div>
                  <div class="modal-body">
                    <p>LoveWorldSAT is a premier Christian television station delivering the best in value-based
                     family content that is inspiring, meaningful, life-changing and informative, broadcasting from 
                     South Africa to the world.
                    </p>
                    <p>LoveWorldSAT was the first Christian television station to beam out of Africa to the rest of 
                    the world, 24/7, nonstop on free-to-air networks. Our satellite coverage beams throughout the 
                    continent of Africa and parts of Europe via Intel Sat 20 satellite. We reach the continent of 
                    Asia via Thaicom 5 C Band satellite covering more than half of the world’s population and also 
                    cater for a global audience via our 24 hour online stream at www.loveworldsat.org, making LoveWorldSAT
                     accessible anywhere in the world
                    </p>
                    <p>Loveworld SAT received the Loveworld Exhibition 2017 Presidential Special Recognition Award </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>




    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>

      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      //var scope = $('[ng-controller="HomeCntrl"]').scope();
      var scope = angular.element($("#appbody")).scope();
      var messages = ['1. Click on ministry arm image to read overview.',
                      '2. Click on the screen to watch promo'
      ]
      //console.log(scope.vm['notiffy'])
      scope.vm['notiffy'](messages);

  });

  </script>

  </body>
</html>
