<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld Exhibition</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
      <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/campusc.png" class="img-fluid rounded mx-auto d-block" usemap="#campc-map">


    <map name="campc-map">
      <area target="" alt="Previous Page" title="Previous Page" href="<?php echo base_url('home/exhibition_hall?q=').'{{vm.user_token}}';?>" coords="483,29,26" shape="circle">
      <area target="" alt="Home" title="Home" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="543,30,22" shape="circle">
      <area target="" alt="Info" title="Info" coords="601,28,21" shape="circle">
      <area target="" alt="Rhapsody Bible Sponsorship" title="Rhapsody Bible Sponsorship" href="<?php echo base_url('home/rhapsody_bible?q=').'{{vm.user_token}}';?>" coords="322,292,537,393" shape="rect">
      <area target="" alt="Rhapsody of Realities" title="Rhapsody of Realities" href="<?php echo base_url('home/rhapsody?q=').'{{vm.user_token}}';?>" coords="648,109,815,268" shape="rect">
      <area target="" alt="Loveworld Publishing Ministry" title="Loveworld Publishing Ministry" href="<?php echo base_url('home/loveworld_publishing?q=').'{{vm.user_token}}';?>" coords="347,90,466,255" shape="rect">
      <area target="" alt="Reachout Campaigns" title="Reachout Campaigns" href="<?php echo base_url('home/reachout_campaigns?q=').'{{vm.user_token}}';?>" coords="490,192,625,273" shape="rect">
      <area target="" alt="Empty" title="Empty" coords="502,102,648,172" shape="rect">
      <area target="" alt="Loveworld Radio" title="Loveworld Radio" href="<?php echo base_url('home/loveworld_radio?q=').'{{vm.user_token}}';?>" coords="550,291,674,400" shape="rect">
      <area target="" alt="Loveworld Television Ministry" title="Loveworld Television Ministry" href="<?php echo base_url('home/loveworld_tm?q=').'{{vm.user_token}}';?>" coords="680,294,823,398" shape="rect">
      <area target="" alt="Loveworld Networks" title="Loveworld Networks" href="<?php echo base_url('home/loveworld_networks?q=').'{{vm.user_token}}';?>" coords="287,403,865,545" shape="rect">
    </map>
loveworld_publishing

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>

      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      });

    </script>

  </body>
</html>
