<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld Exhibition Hall</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
      <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/ehallgate.png" class="img-fluid rounded mx-auto d-block" usemap="#shubgate-map">


    <map name="shubgate-map">
      <!-- <area target="_blank" alt="Seminar hub live stream link" title="Seminar hub live stream link" href="<?php echo base_url('home/hub_live_stream?q=').'{{vm.user_token}}';?>" coords="577,529,374,470" shape="rect"> -->
      <area target="" alt="Campus A" title="Campus A" href="<?php echo base_url('home/campus_a?q=').'{{vm.user_token}}';?>" coords="246,316,441,375" shape="rect">
      <area target="" alt="Campus B" title="Campus B" href="<?php echo base_url('home/campus_b?q=').'{{vm.user_token}}';?>" coords="475,316,661,375" shape="rect">
      <area target="" alt="Campus C" title="Campus C" href="<?php echo base_url('home/campus_c?q=').'{{vm.user_token}}';?>" coords="703,315,894,376" shape="rect">
      <area target="" alt="Back to reception" title="Back to reception" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="455,31,26" shape="circle">
      <area target="" alt="Home" title="Home" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="518,34,29" shape="circle">
      <area target="" alt="Share" title="Share" coords="591,31,29" shape="circle">
      <area target="" alt="Exhibition Hall Info" title="Exhibition Hall Info" coords="672,33,29" shape="circle">
    </map>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>

      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      });

    </script>

  </body>
</html>
