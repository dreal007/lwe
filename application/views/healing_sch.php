<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld Exhibition | Healing School</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/ionicons.min.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
    <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/healingschool.png" class="img-fluid rounded mx-auto d-block" usemap="#campa-map">
      <map name="campa-map">
        <area target="" alt="Previous Page" title="Previous Page" href="<?php echo base_url('home/campus_a?q=').'{{vm.user_token}}';?>" coords="456,27,25" shape="circle">
        <area target="" alt="Home" title="Home" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="516,26,24" shape="circle">
        <area target="" alt="Share Link" title="Share Link" coords="586,25,25" shape="circle">
        <area target="" alt="Info" title="Info" coords="661,25,19" shape="circle">
        <area target="" alt="Click to Watch Promo" title="Click to Watch Promo" href="promo" data-toggle="modal" data-target="#promo" coords="439,295,686,446" shape="rect">
        <area target="" alt="Chat" title="Chat" ng-click="vm.activateChat()" coords="211,646,383,694" shape="rect">
        <area target="" alt="Contact" title="Contact" href="contact" coords="568,695,390,652" data-toggle="modal" data-target="#contact" shape="rect">
        <area target="" alt="Sign Up For Healing school" title="Sign Up For Healing School " ng-click="vm.signup('healing_school')" coords="572,651,746,695" shape="rect">
        <area target="" alt="Downloads" title="Downloads" href="<?php echo base_url('home/download/healing_sch');?>" coords="754,652,920,695" shape="rect">
        <area target="" alt="Brief Overview" title="Brief Overview" href="#" data-target="#overview" data-toggle="modal" coords="148,176,659,259" shape="rect">
        <area target="" alt="Brief Overview" title="Brief Overview" href="#" data-target="#overview" data-toggle="modal" coords="696,503,909,616" shape="rect">
      </map>

      <!--Promo Modal -->
      <div class="modal fade" id="promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <!-- <div class="modal-header">
                  <h5 class="modal-title" style="margin: 0 auto;">Watch Promo</h5>
                </div> -->
                  <div class="modal-body">
                  <div class="embed-responsive embed-responsive-16by9">
                      <video  controls id="innercity" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
                        <source src="<?php echo base_url();?>assets/videos/healingsch.mp4" type="video/mp4">
                      </video>
                  </div>
                  </div>
                  <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" ng-disabled="enterform.$invalid" class="btn btn-primary">Exit</button>
                  </div> -->
              </div>

          </div>
      </div>

      <!--Overview Modal -->
      <div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">Contact Us @ Healing School</h5>
                </div>
                  <div class="modal-body">
                    <p style="font-size:18px;"></p>
                      <strong>Address</strong> : 21/23 Billings Way, Oregun Industrial Estate, Ikeja, Lagos, Nigeria <br>
                      <strong>Phone  </strong> : +234 808 027 2995 <br>
                      <strong>Email  </strong> : feedback@enterthehealingschool.org <br>
                      <strong>Website</strong> : https://www.enterthehealingschool.org

                    </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>

       <!--Overview Modal -->
       <div class="modal fade" id="overview" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">The Healing School</h5>
                </div>
                  <div class="modal-body">
                    <p>The Healing School is a healing ministry of Rev Chris Oyakhilome (D.Sc,. D:D.), 
                    which takes divine healing to the nations.
                    </p>
                    <p>Each year, the Healing School hosts Healing Sessions in Johannesburg, South Africa
                     and Toronto, Canada, where thousands receive their healing and miracles in an atmosphere
                      of faith and the manifest presence of the Holy Spirit.
                    </p>Visiting ministers from around the world also come to the Healing School through the
                     special Ministers' Visitation Program, which affords them the opportunity to witness the
                      miraculous works of the Spirit in our day.
                    <p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>




    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>

      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      //var scope = $('[ng-controller="HomeCntrl"]').scope();
      var scope = angular.element($("#appbody")).scope();
      var messages = ['1. Click on ministry arm image to read overview.',
                      '2. Click on the screen to watch promo'
      ]
      //console.log(scope.vm['notiffy'])
      scope.vm['notiffy'](messages);

  });

  </script>

  </body>
</html>
