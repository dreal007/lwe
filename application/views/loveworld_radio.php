<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld Radio</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/ionicons.min.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
    <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/lwradio.png" class="img-fluid rounded mx-auto d-block" usemap="#campb-map">
      <map name="campb-map">
        <area target="" alt="Previous Page" title="Previous Page" href="<?php echo base_url('home/campus_c?q=').'{{vm.user_token}}';?>" coords="463,27,25" shape="circle">
        <area target="" alt="Home" title="Home" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="521,28,23" shape="circle">
        <area target="" alt="Share Link" title="Share Link" coords="595,27,25" shape="circle">
        <area target="" alt="Info" title="Info" coords="664,28,23" shape="circle">
        <area target="" alt="Click to Watch Promo" title="Click to Watch Promo" href="promo" data-toggle="modal" data-target="#promo" coords="463,324,693,476" shape="rect">
        <area target="" alt="Chat" title="Chat" ng-click="vm.activateChat()" coords="224,654,384,700" shape="rect">
        <area target="" alt="Contact" title="Contact" coords="394,654,566,701" data-toggle="modal" data-target="#contact" shape="rect">
        <area target="" alt="Sign Up For Loveworld Radio" title="Sign Up For Loveworld Radio " ng-click="vm.signup('loveworld_radio')" coords="577,656,750,701" shape="rect">
        <area target="" alt="Downloads" title="Downloads" href="<?php echo base_url('home/download/lwradio');?>" coords="760,654,925,702" shape="rect">
        <area target="" alt="Brief Overview" title="Brief Overview" data-target="#overview" data-toggle="modal" coords="291,166,588,293" shape="rect">
        <area target="" alt="Brief Overview" title="Brief Overview" data-target="#overview" data-toggle="modal" coords="636,185,856,286" shape="rect">
        <area target="" alt="Brief Overview" title="Brief Overview" data-target="#overview" data-toggle="modal" coords="579,528,691,608" shape="rect">
      </map>

      <!--Promo Modal -->
      <div class="modal fade" id="promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                  <div class="modal-body">
                  <div class="embed-responsive embed-responsive-16by9">
                      <video  controls id="innercity" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
                        <source src="http://uflix.s3.amazonaws.com/1581501803-channel.mp4" type="video/mp4">
                      </video>
                  </div>
                  </div>
              </div>

          </div>
      </div>

      <!--Overview Modal -->
      <div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">Contact Us @ Loveworld Radio</h5>
                </div>
                  <div class="modal-body">
                    <p style="font-size:18px;"></p>
                      <strong>Address</strong> : 51/53 Kudirat Abiola Way, Oregun Ikeja, Lagos, Nigeria. <br>
                      <strong>Phone  </strong> : (+)2348123445787<br>
                      <strong>Email  </strong> : loveworldinternetradio@loveworld360.com  <br>
                      <strong>Website</strong> : http://www.loveworldinternetradio.org 

                    </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>

       <!--Overview Modal -->
       <div class="modal fade" id="overview" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">Loveworld Radio</h5>
                </div>
                  <div class="modal-body">
                    <p>LoveWorld Radio is a unique platform of the Believers' Loveworld Ministry, Inc. (Christ Embassy). It’s been estimated that over 80% of the World’s population 
                    is on the internet. With this increasing percentage of internet users the need to influence people with the word of God became vital.
                    </p>
                    <p>www.loveworldradio.fm was birthed by the Holy spirit as a unique platform to reach out to millions of people globally without restrictions.
                     With this innovation, millions of people around the world whose major means of receiving the gospel is via the internet due to religious restrictions.</p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>




    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>

      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      //var scope = $('[ng-controller="HomeCntrl"]').scope();
      var scope = angular.element($("#appbody")).scope();
      var messages = ['1. Click on ministry arm image to read overview.',
                      '2. Click on the screen to watch promo'
      ]
      //console.log(scope.vm['notiffy'])
      scope.vm['notiffy'](messages);

  });

  </script>

  </body>
</html>
