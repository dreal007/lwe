<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Global Youth Leaders' Forum</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/ionicons.min.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
    <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/gylf.png" class="img-fluid rounded mx-auto d-block" usemap="#campb-map">
      <map name="campb-map">
        <area target="" alt="Previous Page" title="Previous Page" href="<?php echo base_url('home/campus_a?q=').'{{vm.user_token}}';?>" coords="463,26,29" shape="circle">
        <area target="" alt="Home" title="Home" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="523,29,25" shape="circle">
        <area target="" alt="Share Link" title="Share Link" coords="592,30,27" shape="circle">
        <area target="" alt="Info" title="Info" coords="666,27,26" shape="circle">
        <area target="" alt="Click to Watch Promo" title="Click to Watch Promo" href="promo" data-toggle="modal" data-target="#promo" coords="774,292,934,398" shape="rect">
        <area target="" alt="Chat" title="Chat" ng-click="vm.activateChat()" coords="219,650,396,695" shape="rect">
        <area target="" alt="Contact" title="Contact" coords="405,649,578,695" data-toggle="modal" data-target="#contact" shape="rect">
        <area target="" alt="Sign Up With GYLF" title="Sign Up With GYLF " ng-click="vm.signup('gylf')" coords="585,649,755,694" shape="rect">
        <area target="" alt="Downloads" title="Downloads" href="<?php echo base_url('home/download/gylf');?>" coords="764,651,919,695" shape="rect">
        <area target="" alt="Brief Overview" title="Brief Overview" data-target="#overview" data-toggle="modal" coords="258,498,346,578" shape="rect">
        <area target="" alt="Brief Overview" title="Brief Overview" data-target="#overview" data-toggle="modal" coords="746,461,873,536" shape="rect">
        <!-- <area target="" alt="Receptionist" title="Receptionist" coords="726,386,673,514,791,516" shape="poly"> -->
      </map>

      <!--Promo Modal -->
      <div class="modal fade" id="promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <!-- <div class="modal-header">
                  <h5 class="modal-title" style="margin: 0 auto;">Watch Promo</h5>
                </div> -->
                  <div class="modal-body">
                  <div class="embed-responsive embed-responsive-16by9">
                      <video  controls id="gylf" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
                        <source src="<?php echo base_url();?>assets/videos/gylf.mp4" type="video/mp4">
                      </video>
                  </div>
                  </div>
                  <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" ng-disabled="enterform.$invalid" class="btn btn-primary">Exit</button>
                  </div> -->
              </div>

          </div>
      </div>

      <!--Overview Modal -->
      <div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">Contact Us @ Global Youth Leaders' Forum</h5>
                </div>
                  <div class="modal-body">
                    <p style="font-size:18px;"></p>
                      <strong>Address</strong> :  <br>
                      <strong>Phone  </strong> :  <br>
                      <strong>Email  </strong> :  <br>
                      <strong>Website</strong> :

                    </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>

       <!--Overview Modal -->
       <div class="modal fade" id="overview" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">The GYLF</h5>
                </div>
                  <div class="modal-body">
                    <p>
                    </p>
                    <p>
                    </p>
                    <p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>




    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>

      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      //var scope = $('[ng-controller="HomeCntrl"]').scope();
      var scope = angular.element($("#appbody")).scope();
      var messages = ['1. Click on ministry arm image to read overview.',
                      '2. Click on the screen to watch promo'
      ]
      //console.log(scope.vm['notiffy'])
      scope.vm['notiffy'](messages);

  });

  </script>

  </body>
</html>
