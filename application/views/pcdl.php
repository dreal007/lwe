<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Pastor Chris Digital Library</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/ionicons.min.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
    <div class="ng-cloak boothback">
      <img src="<?php echo base_url();?>assets/images/pcdl.png" class="img-fluid rounded mx-auto d-block" usemap="#campa-map">
      <map name="campa-map">
        <area target="" alt="Previous Page" title="Previous Page" href="<?php echo base_url('home/campus_a?q=').'{{vm.user_token}}';?>" coords="461,27,28" shape="circle">
        <area target="" alt="Home" title="Home" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="523,27,27" shape="circle">
        <area target="" alt="Share" title="Share" coords="592,25,21" shape="circle">
        <area target="" alt="Info" title="Info" coords="665,27,24" shape="circle">
        <area target="" alt="Watch promo 1" title="Watch promo 1" ng-click="vm.showVideoModal('kingsconf')" coords="158,335,302,438" shape="rect">
        <area target="" alt="Watch promo 2" title="Watch promo 2" ng-click="vm.showVideoModal('pcdl')" coords="318,337,465,439" shape="rect">
        <area target="" alt="Watch promo 3" title="Watch promo 3" ng-click="vm.showVideoModal('kingsu')" coords="521,313,670,415" shape="rect">
        <area target="" alt="Watch promo 4" title="Watch promo 4" ng-click="vm.showVideoModal('kingserv')" coords="682,315,829,418" shape="rect">
        <area target="" alt="Watch promo 5" title="Watch promo 5" ng-click="vm.showVideoModal('kingspay')" coords="842,316,986,414" shape="rect">
        <area target="" alt="Overview" title="Overview" coords="405,184,1075,297" shape="rect">
        <area target="" alt="Kings Chat" title="Kings Chat" coords="176,526,296,614" shape="rect">
        <area target="" alt="Pastor Chris Digital Library" title="Pastor Chris Digital Library" coords="364,530,484,618" shape="rect">
        <area target="" alt="Kings Chat Super User Accounts" title="Kings Chat Super User Accounts" coords="541,530,656,619" shape="rect">
        <area target="" alt="Pastor Chris Online" title="Pastor Chris Online" coords="715,532,825,618" shape="rect">
        <area target="" alt="Pastor Chris Live" title="Pastor Chris Live"  coords="885,534,999,618" shape="rect">
        <area target="" alt="Chat" title="Chat" ng-click="vm.activateChat()" coords="224,654,385,702" shape="rect">
        <area target="" alt="Contact" title="Contact" coords="395,652,570,701" shape="rect">
        <area target="" alt="Sign Up" title="Sign Up" ng-click="vm.signup('pcdl')" coords="578,656,753,702" shape="rect">
        <area target="" alt="Downloads" title="Downloads"  href="<?php echo base_url('home/download/pcdl');?>" coords="759,654,924,703" shape="rect">
      </map>

      <!--Promo Modal -->
      <div class="modal fade" id="promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <!-- <div class="modal-header">
                  <h5 class="modal-title" style="margin: 0 auto;">Watch Promo</h5>
                </div> -->
                  <div class="modal-body">
                  <div class="embed-responsive embed-responsive-16by9">
                      <video  controls id="innercity" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
                        <source type="video/mp4">
                      </video>
                  </div>
                  </div>
                  <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" ng-disabled="enterform.$invalid" class="btn btn-primary">Exit</button>
                  </div> -->
              </div>

          </div>
      </div>

      <!--Overview Modal -->
      <div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">Contact Us @ Loveworld Television Ministry</h5>
                </div>
                  <div class="modal-body">
                    <p style="font-size:18px;"></p>
                      <strong>Address</strong> : 51/53 Kudirat Abiola Way, Oregun Ikeja, Lagos, Nigeria.  <br>
                      <strong>Phone  </strong> : +234 8086801141, +234 8023324188 <br>
                      <strong>Email  </strong> : info@loveworldtelevisionministry.org  <br>
                      <strong>Website</strong> : https://loveworldtelevisionministry.org

                    </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>

       <!--Overview Modal -->
       <div class="modal fade" id="overview" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">The Loveworld Television Ministry</h5>
                </div>
                  <div class="modal-body">
                    <p>The Loveworld Television Ministry is a special ministry of Pastor Chris, armed with the mandate of
                     networking every home around the World with the divine presence of God through TV Transmission.
                    </p>
                    <p>The LoveWorld Television Ministry has grown from airing our flagship programs on only one TV Station
                     during the mid-nineties to broadcasting our programs in all the continents of the world, and currently,
                      we are on over 480 TV Stations reaching over 180 nations worldwide; amongst these are stations with coast
                       to coast coverage
                    </p>
                    <p>The LoveWorld Television Ministry transmits the Monthly Edition of Pastor Chris Live to a Global audience
                     via TV Networks in several nations of the World and on our Web, Mobile and IPTV. </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>




    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>

      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      //var scope = $('[ng-controller="HomeCntrl"]').scope();
      var scope = angular.element($("#appbody")).scope();
      var messages = ['1. Click on ministry arm image to read overview.',
                      '2. Click on the screen to watch promo'
      ]
      //console.log(scope.vm['notiffy'])
      scope.vm['notiffy'](messages);

  });

  </script>

  </body>
</html>
