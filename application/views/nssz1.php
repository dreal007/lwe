<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld Exhibition | Nigeria South South Zone 1</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/ionicons.min.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
    <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/nssz1.png" class="img-fluid rounded mx-auto d-block" usemap="#nssz1">
      <map name="nssz1">
        <area target="" alt="Previous Page" title="Previous Page" href="<?php echo base_url('home/exhibition_map?q=').'{{vm.user_token}}';?>" coords="966,61,51" shape="circle">
        <area target="" alt="Home" title="Home" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="1086,73,51" shape="circle">
        <area target="" alt="Share Link" title="Share Link" coords="1241,59,51" shape="circle">
        <area target="" alt="Info" title="Info" coords="1387,63,51" shape="circle">
        <area target="" alt="Click to Watch Promo" title="Click to Watch Promo" href="promo" data-toggle="modal" data-target="#promo" coords="1553,671,1890,902" shape="rect">
        <area target="" alt="Chat" title="Chat" ng-click="vm.activateChat()" coords="485,1375,808,1470" shape="rect">
        <area target="" alt="Contact" title="Contact" coords="827,1375,1175,1470" data-toggle="modal" data-target="#contact" shape="rect">
        <area target="" alt="Sign Up With Nigeria South South zone 1" title="Sign Up With Nigeria South South zone 1" ng-click="vm.signup('nss_zone_one')" coords="1206,1375,1554,1470" shape="rect">
        <area target="" alt="Downloads" title="Downloads" href="<?php echo base_url('home/download/nss_zone_1');?>" coords="1579,1377,1927,1472" shape="rect">
        <area target="" alt="Brief Overview" title="Brief Overview" data-target="#overview" data-toggle="modal" coords="412,963,749,1234" shape="rect">
      </map>

      <!--Promo Modal -->
      <div class="modal fade" id="promo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <!-- <div class="modal-header">
                  <h5 class="modal-title" style="margin: 0 auto;">Watch Promo</h5>
                </div> -->
                  <div class="modal-body">
                  <div class="embed-responsive embed-responsive-16by9">
                      <video  controls id="nss_zone_1" style="width: 100%; height: auto; margin:0 auto; frameborder:0;">
                        <!-- <source src="<?php echo base_url();?>assets/videos/innercity.mp4" type="video/mp4"> -->
                      </video>
                  </div>
                  </div>
                  <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" ng-disabled="enterform.$invalid" class="btn btn-primary">Exit</button>
                  </div> -->
              </div>

          </div>
      </div>

      <!--Overview Modal -->
      <div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">Contact Us @ Nigeria South South Zone 1</h5>
                </div>
                  <div class="modal-body">
                    <p style="font-size:18px;"></p>
                      <strong>Address</strong> : Sani Abacha Way. Besides Fidelity Bank, Amarata Yenagoa Bayelsa State <br>
                      <strong>Phone  </strong> : +234(0) 8037657160, 07038470296 <br>
                      <strong>Email  </strong> : info@cebayelsa.com <br>
                      <strong>Website</strong> : www.cebayelsa.com

                    </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>

       <!--Overview Modal -->
       <div class="modal fade" id="overview" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" style="margin: 0 auto;">Nigeria South South Zone 1</h5>
                </div>
                  <div class="modal-body">
                    <p> 
                        Nigeria South South Zone 1 is made up of CE Bayelsa, CE Ughelli and CE Sapele (The moving B.U.S)
                         situated in the Niger delta region of Nigeria with over 140 churches and a thriving membership of over 15 000 people.
                        The cell ministry year was phenomenal and amazing; we recorded amazing testimonies and so much growth. 
                        We had outreaches inclusive of The Night of Bliss Ughelli, The Bayelsa Teens Fire Conference, 
                        The Love and Laughter Concert, The PCF crusades, Skills acquisition outreaches, Schools outreaches, 
                        Do you believe in miracles outreach, The Ughelli youth Conference, moments of laughter, 
                        The power of your mind breakfast outreach, 
                    </p>
                    <p>The Super Sunday with Pastor Obi and so many more. We reached out to over 25,000 souls and graduated 
                      8,000 people from the foundation school, baptizing them on different occasions. We distributed our 
                      Ministry Materials in different formats during our outreaches and program having special outreaches 
                      such as the Healing from Heaven outreach, None of These Disease Hospitals Outreach, and lots more
                    </p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>
              </div>

          </div>
      </div>




    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>

      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      //var scope = $('[ng-controller="HomeCntrl"]').scope();
      var scope = angular.element($("#appbody")).scope();
      var messages = ['1. Click on ministry arm image to read overview.',
                      '2. Click on the screen to watch promo'
      ]
      //console.log(scope.vm['notiffy'])
      scope.vm['notiffy'](messages);

  });

  </script>

  </body>
</html>
