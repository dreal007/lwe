<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Virtual Loveworld Exhibition Hall</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap and Font Awesome css-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/countdown/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/countdown/css/bootstrap.min.css">
    <!-- Google fonts-->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,700">
    <!-- Theme stylesheet-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/countdown/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/countdown/css/custom.css">
    <!-- Favicon-->

    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div style="background-image: url('<?php echo base_url();?>assets/countdown/img/beach.jpg')" class="main"> 
      <div class="overlay"></div>
      <div class="container">
       
          <div class="col-sm-12">
            <h3 class="cursive text-left">Welcome To</h3>
            <h2 class="sub">Virtual Loveworld Exhibition Hall</h2>
          </div>
        </div>
        <!-- countdown-->
        <div id="countdown" class="countdown">
          <div class="row">
            <div class="countdown-item col-sm-3 col-xs-3">
              <div id="countdown-days" class="countdown-number">&nbsp;</div>
              <div class="countdown-label">Days</div>
            </div>
            <div class="countdown-item col-sm-3 col-xs-3">
              <div id="countdown-hours" class="countdown-number">&nbsp;</div>
              <div class="countdown-label">Hrs</div>
            </div>
            <div class="countdown-item col-sm-3 col-xs-3">
              <div id="countdown-minutes" class="countdown-number">&nbsp;</div>
              <div class="countdown-label">Mins</div>
            </div>
            <div class="countdown-item col-sm-3 col-xs-3">
              <div id="countdown-seconds" class="countdown-number">&nbsp;</div>
              <div class="countdown-label">Secs</div>
            </div>
          </div>
        </div>
        <!-- /.countdown-->
        <!-- <div class="mailing-list">
          <h3 class="mailing-list-heading">The Hall will be open soon</h3>
          <div class="row">
           
          </div>
        </div> -->
      </div>
      <div class="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy;2020 Virtual Loveworld Exhibition</p>
            </div>
            <div class="col-md-6">
              <!-- please do not remove credit to us, if you need to remove the link please donate to support further theme's development-->
              <!-- <p class="credit">Template by <a href="https://bootstrapious.com/landing-pages">Bootstrapious</a><br />with support from <a href="https://fity.cz/">Fity</a></p> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- JAVASCRIPT FILES -->
    <script src="<?php echo base_url();?>assets/countdown/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/countdown/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/countdown/js/jquery.cookie.js"></script>
    <script src="<?php echo base_url();?>assets/countdown/js/jquery.countdown.min.js"></script>
    <script src="<?php echo base_url();?>assets/countdown/js/front.js"></script>
  </body>
</html>