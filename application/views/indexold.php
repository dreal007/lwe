<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="CoinCup | Home" />
  <meta name="description" content="CoinCup | Home">
  <meta name="author" content="CoinCup | Home">
  <title>CoinCup | Home </title>

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/vendor/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/vendor/font-awesome/css/font-awesome.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/vendor/simple-line-icons/css/simple-line-icons.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/vendor/magnific-popup/magnific-popup.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/theme-shop.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/theme-animate.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/css/settings.css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/css/layers.css" media="screen">
		<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css" media="screen"> 

		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/vendor/circle-flip-slideshow/css/component.css" media="screen">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/frontend/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url();?>assets/frontend/vendor/modernizr/modernizr.js"></script>

		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="vendor/respond/respond.js"></script>
			<script src="vendor/excanvas/excanvas.js"></script>
		<![endif]-->

	</head>
	<body>
		<div class="body">
			<header id="header" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 57, "stickySetTop": "-57px", "stickyChangeLogo": true}'>
				<div class="header-body">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-logo">
									<a href="index.html">
										<img alt="Porto" width="167" height="39" data-sticky-width="82" data-sticky-height="40" data-sticky-top="33" src="<?php echo base_url(); ?>assets/img/fulllogo.jpg">
									</a>
								</div>
							</div>
							<div class="header-column">
								<div class="header-row">
									<div class="header-nav">
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
											<i class="fa fa-bars"></i>
										</button>
										<ul class="header-social-icons social-icons hidden-xs">
											<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
											<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
										</ul>
										<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
											<nav>
												<ul class="nav nav-pills" id="mainNav">
													<li class="active">
														<a class="" href="<?php echo base_url('home');?>">
															Home
														</a>
													</li>
													<li class="">
														<a class="" href="<?php echo base_url('home/register');?>">
															Register
														</a>
													</li>
													<li class="">
														<a class="" href="<?php echo base_url('home/login');?>">
															Login
														</a>
													</li>
													
													<li class="">
														<a class="" href="<?php echo base_url('home/contact');?>">
															Contact Us
														</a>
														<ul class="dropdown-menu">
															<li><a href="contact-us.html">Contact Us - Basic</a></li>
															<li><a href="contact-us-advanced.php">Contact Us - Advanced</a></li>
														</ul>
													</li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div role="main" class="main">
				<div class="slider-container rev_slider_wrapper" style="height: 700px;">
					<div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"gridwidth": 800, "gridheight": 700}'>
						<ul>
							<li data-transition="fade">
								<img src="<?php echo base_url();?>/assets/img/cc2mod.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat" 
									class="rev-slidebg">


								<div class="tp-caption top-label"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="-95"
									data-start="300"
									style="z-index: 5"
									data-transform_in="y:[-300%];opacity:0;s:500;">DO YOU NEED A STEADY AND REALIABLE</div>

								<div class="tp-caption main-label"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="-45"
									data-start="1500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">SOURCE OF INCOME?</div>

								<div class="tp-caption bottom-label"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="5"
									data-start="1000"
									style="z-index: 5"
									data-transform_in="y:[100%];opacity:0;s:500;">Don't wait any second longer.</div>

								<a class="tp-caption btn btn-lg btn-primary btn-slider-action"
									data-hash
									data-hash-offset="85"
									href="#plans"
									data-x="center" data-hoffset="0"
									data-y="center" data-voffset="80"
									data-start="2200"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Get Started Now!</a>
									
									
								
							</li>
						</ul>
					</div>
				</div>
				<div class="home-intro" id="home-intro">
					<div class="container">

						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<p>
									The fastest way to create your dream <em>Income and Wealth</em>
									<span class="text-center">Check out our donation plans and packages.</span>
								</p>
							</div>
						</div>

					</div>
				</div>
				<div class="container">

					<div class="row">
						<div class="col-md-12 center">
							<h2 class="word-rotator-title mb-sm">CoinCup is the <strong>most <span class="word-rotate" data-plugin-options='{"delay": 2000, "animDelay": 300}'>
								<span class="word-rotate-items">
									<span>popular</span>
									<span>reliable</span>
									<span>incredible</span>
								</span>
							</span></strong> Peer to Peer Site On the Web.</h2>
							<p class="lead">Trusted by over 134+ satisfied members, Coincup is a huge success<br>in helping it's members create the kind of wealth they desire!.</p>
						</div>
					</div>

					<div class="row mt-xl">
						<div class="counters counters-text-dark">
							<div class="col-md-3 col-sm-6">
								<div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
									<i class="fa fa-user"></i>
									<strong data-to="134" data-append="+">0</strong>
									<label>Happy Members</label>
									<p class="text-color-primary mb-xl">They can’t be wrong</p>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600">
									<i class="fa fa-desktop"></i>
									<strong data-to="5">0</strong>
									<label>Donation Packages to choose from</label>
									<p class="text-color-primary mb-xl">Many more to come</p>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="900">
									<i class="fa fa-money"></i>
									<strong data-to="248" data-append="+">0</strong>
									<label>Donations Paid</label>
									<p class="text-color-primary mb-xl">Satisfaction guaranteed</p>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="1200">
									<i class="fa fa-clock-o"></i>
									<strong data-to="48" data-append="+">0</strong>
									<label>Payment Hours</label>
									<p class="text-color-primary mb-xl">Join now and make real money!</p>
								</div>
							</div>
						</div>
					</div>

				</div>
				
				<section class="call-to-action call-to-action-primary mb-none">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="call-to-action-content align-centre pb-md mb-xl ml-none">
									<h2 class="text-color-light mb-none mt-xl">Coincup is ready to make you as <strong>wealthy as you choose</strong></h2>
									<p class="lead mb-xl text-center">Here are our donation packages you can choose from:</p> 
								</div>
							</div>
						</div>
					</div>
				</section><br>
			
			<section>
			  <div class="container plans" id="plans">
				<div class="row">
						<div class="pricing-table">
							<div class="col-lg-4 col-sm-6">
								<div class="plan">
									<h3>Basic<span>&#8358 12,500</span></h3>
									<a class="btn btn-md btn-primary" href="<?php echo base_url('home/login');?>">Sign up</a><br><br>
									<ul>
										<li><b>&#8358 12,500</b> Donation</li>
										<li><b>&#8358 25,000</b> Reward</li>
										<li><b>100%</b> Return on Investment</li>
										<li><b>48Hrs</b> Return Time</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6">
								<div class="plan">
									<h3>Elite<span>&#8358 25,000</span></h3>
									<a class="btn btn-md btn-primary" href="<?php echo base_url('home/login');?>">Sign up</a><br><br>
									<ul>
										<li><b>&#8358 25,000</b> Donation</li>
										<li><b>&#8358 50,000</b> Reward</li>
										<li><b>100%</b> Return on Investment</li>
										<li><b>48Hrs</b> Return Time</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-4 col-sm-6">
								<div class="plan">
									<h3>Standard<span>&#8358 50,000</span></h3>
									<a class="btn btn-md btn-primary" href="<?php echo base_url('home/login');?>">Sign up</a><br><br>
									<ul>
										<li><b>&#8358 50,000</b> Donation</li>
										<li><b>&#8358 100,000</b> Reward</li>
										<li><b>100%</b> Return on Investment</li>
										<li><b>48Hrs</b> Return Time</li>
									</ul>
								</div>
							  </div>
							</div>
						  </div>
						 </div>
					 <div class="container">
						<div class="row">
						 <div class="pricing-table">
							<div class="col-lg-4 col-sm-6 col-lg-offset-2">
								<div class="plan most-popular">
									<div class="plan-ribbon-wrapper"><div class="plan-ribbon">Popular</div></div>
									<h3>Professional<span>&#8358 100,000</span></h3>
									<a class="btn btn-md btn-warning" href="<?php echo base_url('home/login');?>">Coming Soon</a><br><br>
									<ul>
										<li><b>&#8358 100,000</b> Donation</li>
										<li><b>&#8358 200,000</b> Reward</li>
										<li><b>100%</b> Return on Investment</li>
										<li><b>48Hrs</b> Return Time</li>
									</ul>
								</div>
							</div>
						   <div class="col-lg-4 col-sm-6">
								<div class="plan most-popular">
								    <div class="plan-ribbon-wrapper"><div class="plan-ribbon">Ultimate</div></div>
									<h3>Enterprise<span>&#8358 200,000</span></h3>
									<a class="btn btn-md btn-warning" href="<?php echo base_url('home/login');?>">Coming Soon</a><br><br>
									<ul>
										<li><b>&#8358 200,000</b> Donation</li>
										<li><b>&#8358 400,000</b> Reward</li>
										<li><b>100%</b> Return on Investment</li>
										<li><b>48Hrs</b> Return Time</li>
									</ul>
								</div>
							</div>
						  </div>
					  </div>
                    </div>
				</section>
			
				
				<div class="container">
					<div class="row">
						<div class="col-md-12 center">
							<h2 class="mt-xl mb-none">Optimized <strong class="alternative-font font-size-md">matching algorithm</strong> <span class="alternative-font font-size-md">...too!</span></h2>
							<p class="lead mb-xl">Our mission is to help every man attain his <strong class="alternative-font font-size-md">Wealth</strong> potential !.</p>
							<hr class="invisible">
						</div>
					</div>
				</div>
				

				</div>
				
				<section class="call-to-action call-to-action-default call-to-action-in-footer mt-none no-top-arrow">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="call-to-action-content align-left pb-md mb-xl ml-none">
									<h2 class="mb-xs mt-xl">Start creating wealth today with <strong class="alternative-font font-size-md">CoinCup!</strong></h2>
		
								</div>
								<div class="call-to-action-btn">
									<a href="<?php echo base_url('home/register'); ?>" class="btn btn-lg btn-primary"><i class="fa fa-cark mr-xs"></i> REGISTER NOW !</a>
			
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
 
			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="footer-ribbon">
							<span>Get in Touch</span>
						</div>
						
						<div class="col-md-3">
							<h4>Latest Tweets</h4>
							<div id="tweet" class="twitter" data-plugin-tweets data-plugin-options='{"username": "", "count": 2}'>
								<p>Please wait...</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="contact-details">
								<h4>Contact Us</h4>
								<ul class="contact">
									<li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="support@coincup.net">support@coincup.net</a></p></li>
								</ul>
							</div>
						</div>
						<div class="col-md-2">
							<h4>Follow Us</h4>
							<ul class="social-icons">
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-md-7">
								<p>© Copyright 2017. All Rights Reserved.</p>
							</div>

						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<!--[if lt IE 9]>
		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="<?php echo base_url();?>assets/frontend/vendor/jquery/jquery.js"></script>
		<!--<![endif]-->
		<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.appear/jquery.appear.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.easing/jquery.easing.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/jquery-cookie/jquery-cookie.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/common/common.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.validation/jquery.validation.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.stellar/jquery.stellar.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.gmap/jquery.gmap.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/jquery.lazyload/jquery.lazyload.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/isotope/jquery.isotope.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/owl.carousel/owl.carousel.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/vide/vide.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url();?>assets/frontend/js/theme.js"></script>
		
		<!-- Specific Page Vendor and Views -->
		<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/extensions/revolution.extension.carousel.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/vendor/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
		
		<script src="<?php echo base_url();?>assets/frontend/vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
		<script src="<?php echo base_url();?>assets/frontend/js/views/view.home.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url();?>assets/frontend/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url();?>assets/frontend/js/theme.init.js"></script>

		

	</body>
</html>