<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld Exhibition</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
      <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/reception.png" class="img-fluid rounded mx-auto d-block" usemap="#reception-map">

    <map name="reception-map">
    <area target="" alt="Exhibition Map" title="Exhibition Map" href="<?php echo base_url('home/exhibition_map?q=').'{{vm.user_token}}';?>" coords="801,472,943,603" shape="rect">
    <area target="" alt="Male Receptionist" title="Male Receptionist" coords="602,436,569,485,561,557,639,555,636,490" shape="poly">
    <area target="" alt="Female Receptionist" title="Female Receptionist" coords="256,456,236,505,236,564,288,561,292,513,277,474" shape="poly">
    <area target="" alt="Exhibition Hall" title="Exhibition Hall" href="<?php echo base_url('home/exhibition_hall?q=').'{{vm.user_token}}';?>" coords="181,242,441,404" shape="rect">
    <area target="" alt="Seminar Hub" title="Seminar Hub" href="<?php echo base_url('home/seminar_hub?q=').'{{vm.user_token}}';?>" coords="1032,240,1298,393" shape="rect">
    <area target="" alt="Screen 1" title="Screen 1" coords="574,324,655,381" shape="rect">
    <area target="" alt="Screen 2" title="Screen 2" coords="694,328,776,377" shape="rect">
    <area target="" alt="Screen 3" title="Screen 3" coords="819,327,900,375" shape="rect">
    <area target="_blank" alt="Exhibition Welcome page" title="Exhibition Welcome page" href="https://loveworldexhibition.online" coords="221,569,638,697" shape="rect">
</map>


    <!--Enter Modal -->
      <div class="modal fade" id="enter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" style="margin: 0 auto;">Enter The Loveworld Exhibition</h5>
                </div>
                <form name="enterform" ng-submit="vm.enter(phone_id);">
                  <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-10 offset-md-1">
                          <label for="phone">Enter Kingschat Number</label>
                          <input type="text" class="form-control" name="phone" id="phone" ng-model="phone_id"  placeholder="+2348076543469" ng-required="true">
                          <small ng-show="enterform.phone.$error.required && enterform.phone.$dirty" class="form-text text-info"> Please provide a registered phone number</small>
                      </div>
                    </div>   
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" ng-disabled="enterform.$invalid" class="btn btn-primary">Enter</button>
                  </div>
                </form> 
              </div>
            
          </div>
        </div>

        <!--Exit Modal -->
      <div class="modal fade" id="exit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" style="margin: 0 auto;">Exit The Loveworld Exhibition</h5>
                </div>
                <form name="enterform" ng-submit="vm.logout();">
                  <!-- <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-10 offset-md-1">
                          <label for="phone">Enter Kingschat Number</label>
                          <input type="text" class="form-control" name="phone" id="phone" ng-model="phone_id"  placeholder="+2348076543469" ng-required="true">
                          <small ng-show="enterform.phone.$error.required && enterform.phone.$dirty" class="form-text text-info"> Please provide a registered phone number</small>
                      </div>
                    </div>   
                  </div> -->
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" ng-disabled="enterform.$invalid" class="btn btn-primary">Exit</button>
                  </div>
                </form> 
              </div>
            
          </div>
        </div>

        <!--Register Modal -->
      <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" style="margin: 0 auto;">Register For The Loveworld Exhibition</h5>
                </div>
                <form name="regform">
                  <div class="modal-body">
                    <div class="form-row">
                      <div class="form-group col-md-10 offset-md-1">
                          <label for="fullname">Full Name</label>
                          <input type="text" class="form-control" name="fullname" ng-model="user.fullname" placeholder="Enter your full name" ng-required="true">
                      </div>
                      <div class="form-group col-md-10 offset-md-1">
                          <label for="email">Email</label>
                          <input type="email" class="form-control" name="email" ng-model="user.email"  placeholder="eddwards@gmail.com" ng-required="true">
                      </div>
                      <div class="form-group col-md-10 offset-md-1">
                          <label for="phone">Enter Kingschat Number</label>
                          <input type="text" class="form-control" name="phone" ng-model="user.phone"  placeholder="+2348076543469" ng-required="true">
                      </div>
                      <div class="form-group col-md-10 offset-md-1">
                          <label for="phone">Gender</label>
                          <select id="inputState" class="form-control" name="gender" ng-model="user.gender"  ng-required="true">
                              <option selected disabled>Choose gender</option>
                              <option value="male">Male</option>
                              <option value="female">Female</option>
                          </select>
                      </div>
                    </div>   
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button ng-disabled="regform.$invalid" type="submit" class="btn btn-primary" ng-click="vm.register(user);">Register</button>
                  </div>
                </form> 
              </div>
            
          </div>
        </div>
  </div>
   

    <!-- <footer class="container">
      <p>&copy; Company 2017-2018</p>
    </footer> -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>
      
      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      });

    </script>

  </body>
</html>
