<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld Exhibition</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
      <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/campusb.png" class="img-fluid rounded mx-auto d-block" usemap="#campb-map">


    <map name="campb-map">
      <area target="" alt="Previous page" title="Previous page" href="<?php echo base_url('home/exhibition_hall?q=').'{{vm.user_token}}';?>" coords="482,27,24" shape="circle">
      <area target="" alt="Home" title="Home" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="543,31,26" shape="circle">
      <area target="" alt="Info" title="Info" coords="603,28,22" shape="circle">
      <area target="" alt="Children's Ministry" title="Children's Ministry" href="<?php echo base_url('home/childrens_ministry?q=').'{{vm.user_token}}';?>" coords="335,82,455,287" shape="rect">
      <area target="" alt="360 Lumine" title="360 Lumine" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="488,98,617,194" shape="rect">
      <area target="" alt="Internet Multimedia" title="Internet Multimedia" href="<?php echo base_url('home/internet_multimedia?q=').'{{vm.user_token}}';?>" coords="646,96,805,187" shape="rect">
      <area target="" alt="Chris Oyakhilome Foundation" title="Chris Oyakhilome Foundation" href="<?php echo base_url('home/cofi?q=').'{{vm.user_token}}';?>" coords="484,296,619,202" shape="rect">
      <area target="" alt="Teens Ministry" title="Teens Ministry" href="<?php echo base_url('home/teens_ministry?q=').'{{vm.user_token}}';?>" coords="318,321,529,474" shape="rect">
      <area target="" alt="Loveworld Audio Visuals" title="Loveworld Audio Visuals" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="546,318,830,474" shape="rect">
      <area target="" alt="The Haven Nation" title="The Haven Nation" href="<?php echo base_url('home/haven?q=').'{{vm.user_token}}';?>" coords="649,201,808,295" shape="rect">
      <area target="" alt="The Innercity Mission" title="The Innercity Mission" href="<?php echo base_url('home/innercity?q=').'{{vm.user_token}}';?>" coords="289,499,857,586" shape="0">
    </map>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>

      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      });

    </script>

  </body>
</html>
