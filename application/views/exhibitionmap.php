<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld Exhibition</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
      <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/exhibitionmap.png" class="img-fluid rounded mx-auto d-block" usemap="#exhibition-map">


    <map name="exhibition-map">
      <!-- <area target="" alt="Campus A" title="Campus A" href="<?php echo base_url('home/campus_a?q=').'{{vm.user_token}}';?>" coords="364,43,546,369" shape="rect">
      <area target="" alt="Campus B" title="Campus B" href="<?php echo base_url('home/campus_b?q=').'{{vm.user_token}}';?>" coords="305,403,585,589" shape="rect">
      <area target="" alt="Campus C" title="Campus C" href="<?php echo base_url('home/campus_c?q=').'{{vm.user_token}}';?>" coords="652,472,899,694" shape="rect"> -->
      
    <area target="" alt="Ceremony" title="Ceremony" href="ceremony" coords="525,73,828,240" shape="rect">
    <area target="" alt="Cyber Suite" title="Cyber Suite" href="cybersuite" coords="127,147,195,281" shape="rect">
    <area target="" alt="Org" title="Org" href="org" coords="198,141,327,223" shape="rect">
    <area target="" alt="Seminar Hub" title="Seminar Hub" href="seminarhub" coords="339,147,519,228" shape="rect">
    <area target="" alt="Accra Ghana" title="Accra Ghana" href="accraghana" coords="836,146,959,229" shape="rect">
    <area target="" alt="USA Region 2" title="USA Region 2" href="usaregion2" coords="961,151,1020,237" shape="rect">
    <area target="" alt="Pacific" title="Pacific" href="pacific" coords="1028,145,1089,233" shape="rect">
    <area target="" alt="Teens" title="Teens" href="teens" coords="1096,139,1164,228" shape="rect">
    <area target="" alt="Lagos Zone 1" title="Lagos Zone 1" href="lagoszone1" coords="101,327,191,455" shape="rect">
    <area target="" alt="UK Zone 1" title="UK Zone 1" href="ukzone1" coords="256,296,390,378" shape="rect">
    <area target="" alt="Church Growth International" title="Church Growth International" href="<?php echo base_url('home/church_growth?q=').'{{vm.user_token}}';?>" coords="399,295,512,375" shape="rect">
    <area target="" alt="Port Harcourt Zone 3" title="Port Harcourt Zone 3" href="phzone3" coords="575,368,702,285,701,368" shape="rect">
    <area target="" alt="Nigeria South South Zone 1" title="Nigeria South South Zone 1" href="<?php echo base_url('home/nss_zone_one?q=').'{{vm.user_token}}';?>" coords="708,287,830,370" shape="rect">
    <area target="" alt="Abuja Ministry Center" title="Abuja Ministry Center" href="abujamin" coords="906,288,1157,372" shape="rect">
    <area target="" alt="STDL" title="STDL" href="stdl" coords="254,387,312,469" shape="rect">
    <area target="" alt="Abeokuta Ministry Center" title="Abeokuta Ministry Center" href="abeokuta" coords="382,390,511,468" shape="rect">
    <area target="" alt="Warri Ministry Center" title="Warri Ministry Center" href="warri" coords="275,526,377,605" shape="rect">
    <area target="" alt="Lagos Zone 2" title="Lagos Zone 2" href="lagoszone2" coords="274,620,389,686" shape="rect">
    <area target="" alt="Port Harcourt Zone 1" title="Port Harcourt Zone 1" href="phzone1" coords="767,531,892,616" shape="rect">
    <area target="" alt="STDL" title="STDL" href="stdl" coords="254,387,312,469" shape="rect">
    
    <area target="" alt="Port Harcourt Zone 1" title="Port Harcourt Zone 1" href="phzone1" coords="767,531,892,616" shape="rect">
    <area target="" alt="Benin Zone 2" title="Benin Zone 2" href="beninzone2" coords="382,532,449,616" shape="rect">
    <area target="" alt="Nigeria South East Zone 1" title="Nigeria South East Zone 1" href="nsezone1" coords="459,530,517,612" shape="rect">
    <area target="" alt="South Africa Zone 1" title="South Africa Zone 1" href="sazone1" coords="520,529,583,610" shape="rect">
    <area target="" alt="Northern region" title="Northern region" href="nregion" coords="392,618,511,693" shape="rect">
    <area target="" alt="South Africa Zone 5" title="South Africa Zone 5" href="sazone5" coords="517,621,578,699" shape="rect">
    <area target="" alt="Loveworld Secretariat" title="Loveworld Secretariat" href="lwsecretariat" coords="589,529,712,693" shape="rect">
    <area target="" alt="Lagos Zone 3" title="Lagos Zone 3" href="lagoszone3" coords="765,621,889,697" shape="rect">
    <area target="" alt="Benin Zone 1" title="Benin Zone 1" href="beninzone1" coords="900,621,1015,691" shape="rect">
    <area target="" alt="Mid West Zone" title="Mid West Zone" href="mwzone" coords="1025,625,1141,692" shape="rect">
    <area target="" alt="Calabar Ministry Center" title="Calabar Ministry Center" href="calabarmin" coords="1044,533,1162,623" shape="rect">
    <area target="" alt="USA Region 1" title="USA Region 1" href="usareg1" coords="972,533,1038,618" shape="rect">
    <area target="" alt="Pre Natal Program" title="Pre Natal Program" href="prenatalprog" coords="899,536,968,615" shape="rect">
    <area target="" alt="NNE Zone 1" title="NNE Zone 1" href="nnezone1" coords="900,385,962,469" shape="rect">
    <area target="" alt="Eastern Europe" title="Eastern Europe" href="easterneurope" coords="707,388,830,470" shape="rect">
    <area target="" alt="LCM" title="LCM" href="lcm" coords="578,389,640,469" shape="rect">
    <area target="" alt="QUEBEC" title="QUEBEC" href="quebec" coords="643,387,702,470" shape="rect">
    <area target="" alt="ECWA" title="ECWA" href="ecwa" coords="966,387,1141,470" shape="rect">
    <area target="" alt="PNDTP" title="PNDTP" href="pndtp" coords="314,387,376,471" shape="rect">
    <area target="" alt="GYLF" title="GYLF" href="gylf" coords="109,460,198,517" shape="rect">
    <area target="" alt="OFTP" title="OFTP" href="oftp" coords="104,519,189,585" shape="rect">
    <area target="" alt="Loveworld Next" title="Loveworld Next" href="lwnext" coords="93,590,189,653" shape="rect">
    <area target="" alt="Org" title="Org" href="org2" coords="132,661,187,765" shape="rect">
    
    </map>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>
      
      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      });

    </script>

  </body>
</html>
