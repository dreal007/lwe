<!doctype html>
<html lang="en" ng-app="app">
  <head>
    <base href="/lwe/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no, maximum-scale=1, minimum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Loveworld Exhibition</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <style type="text/css">
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
          display: none !important;
      }
    </style>
  </head>

<body class="background" ng-controller="HomeCntrl as vm" id="appbody">
      <div class="ng-cloak">
      <img src="<?php echo base_url();?>assets/images/campusa.png" class="img-fluid rounded mx-auto d-block" usemap="#campa-map">


    <map name="campa-map">
      <area target="" alt="Healing School" title="Healing School" href="<?php echo base_url('home/healing_school?q=').'{{vm.user_token}}';?>" coords="353,113,814,186" shape="rect">
      <area target="" alt="Campus Ministry" title="Campus Ministry" href="<?php echo base_url('home/campus_ministry?q=').'{{vm.user_token}}';?>" coords="324,482,544,653" shape="rect">
      <area target="" alt="International School of Ministry" title="International School of Ministry" href="<?php echo base_url('home/ism?q=').'{{vm.user_token}}';?>" coords="559,478,851,649" shape="rect">
      <area target="" alt="Seminar Hub" title="Seminar Hub" href="<?php echo base_url('home/seminar_hub?q=').'{{vm.user_token}}';?>" coords="722,213,835,446" shape="rect">
      <area target="" alt="Pastor Chris Live" title="Pastor Chris Live" href="<?php echo base_url('home/pcdl?q=').'{{vm.user_token}}';?>" coords="487,213,607,451" shape="rect">
      <area target="" alt="Loveworld Medical Missions" title="Loveworld Medical Missions" href="<?php echo base_url('home/medical_missions?q=').'{{vm.user_token}}';?>" coords="348,211,462,444" shape="rect">
      <area target="" alt="Global Youth Leaders Forum" title="Global Youth Leaders Forum" href="<?php echo base_url('home/gylf?q=').'{{vm.user_token}}';?>" coords="624,212,702,311" shape="rect">
      <area target="" alt="Future Africa Leaders Foundation" title="Future Africa Leaders Foundation" href="<?php echo base_url('home/falf?q=').'{{vm.user_token}}';?>" coords="627,332,703,445" shape="rect">
      <area target="" alt="Previous Page" title="Previous Page" href="<?php echo base_url('home/exhibition_hall?q=').'{{vm.user_token}}';?>" coords="486,27,24" shape="circle">
      <area target="" alt="Home" title="Home" href="<?php echo base_url('home/reception?q=').'{{vm.user_token}}';?>" coords="544,31,24" shape="circle">
      <area target="" alt="Info" title="Info" coords="606,27,26" shape="circle">
    </map>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/imagemap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/rotate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-notify.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="<?php echo base_url();?>assets/bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/homeCntrl.js"></script>
    <script>

      $(document).ready(function() {
         $('map').imageMapResize();

         var PleaseRotateOptions = {
          startOnPageLoad: true,
          onHide: function(){},
          onShow: function(){},
          forceLandscape: false,
          message: "Please Rotate Your Device for best experience",
          subMessage: "(or click to continue)",
          allowClickBypass: true,
          onlyMobile: true,
          zIndex: 1000,
          iconNode: null
      };

      });

    </script>

  </body>
</html>
