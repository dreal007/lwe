<?php

class Token {

 protected $CI;
 
  
  public function __construct(){
     $this->CI =& get_instance();
	 $this->CI->load->library('session');
   }
   
    public function generate() {
        $tok = sha1(md5(openssl_random_pseudo_bytes(64)));
        $this->CI->session->token = $tok ;
        return $tok; 
    }
	
	public function validate(){
	    if ($this->CI->input->post('token') === $this->CI->session->token){
		    unset($this->CI->session->token);
			return true;
		}
        else {
		    unset($this->CI->session->token);
			return false;
		}		
	
	}
    
}
