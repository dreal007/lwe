<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recaptcha {

        public function run(){
          
		if(isset($_POST['g-recaptcha-response'])){
		  $curl = curl_init();
		  curl_setopt_array($curl,[
		  CURLOPT_RETURNTRANSFER => 1,
		  CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
		  CURLOPT_POST => 1,
		  CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_POSTFIELDS => [
		    'secret'   => '6LdZpBYUAAAAAN_iDVkrmNlL5brtN4Wr7l4L2PLg',
			'response' => $_POST['g-recaptcha-response'],
		  ],
		  
		  ]);
		
		  $response = json_decode(curl_exec($curl));
		  
	/* $secret = '6LdZpBYUAAAAAN_iDVkrmNlL5brtN4Wr7l4L2PLg';
	$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.
	$secret.'&response='.$_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);
    if($responseData->success) */
		  
		  if($response->success)
		  return true;
		  else 
		  return false;
		  
		 }   
		  
	}	  
        
}