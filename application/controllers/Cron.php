<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {


    public function __construct() {
        parent::__construct();
    }
	
	public function insertgd(){
	  //if($this->input->is_cli_request()){
	    if ($this->session->AdminIsLogged == 'yes' ){
	       $this->cron_model->insert_gd();
		   redirect('admin/dashboard' , 'refresh');
		}
        else  $this->load->view('index');
		}  		
	 //}
	
	public function merge(){
	  //if($this->input->is_cli_request()){
	  if ($this->session->AdminIsLogged == 'yes' ){
	      $this->cron_model->merge();
		redirect('admin/dashboard' , 'refresh');
		}
       else  $this->load->view('index');		
	  //}
	} 
	
	public function suspend(){
	   $this->cron_model->suspend();
	}
//cron command
//$sql = "SELECT id, username FROM users WHERE signup<=CURRENT_DATE - INTERVAL 3 DAY AND activated='0'";
//

}
