<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        /* cache control */
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");
        $this->load->library('Recaptcha');
        $this->load->library('Token');
    }

    public function index() {
        if ($this->session->UserIsLogged == 1)
            redirect('home/dashboard', 'refresh');
        else
            $this->load->view('index');
    }

    public function confirm() {
        if (!empty($this->input->post())) {
            $data['phmd_id'] = $this->input->post('phmd_id');
            $data['phu_id'] = $this->input->post('phu_id');
            $data['gd_id'] = $this->input->post('gd_id');
            $response = $this->user_model->confirm($data);
            redirect('user/pendingApp');
        }
    }

    public function cancel() {
        if (!empty($this->input->post())) {
            $data['id'] = $this->input->post('id');
            $data['user_id'] = $this->input->post('user_id');
            $data['category'] = $this->input->post('cat');
            $data['merged'] = $this->input->post('merged');
            $data['confirmed'] = $this->input->post('confirmed');
            $data['status'] = $this->input->post('status');
            $response = $this->user_model->cancel($data);
            redirect('user/history');
        }
    }

    public function viewImg($img = '') {
        $data['img_path'] = base_url('assets/uploads/pics/') . $img;
        $this->load->view('view_proof', $data);
    }

    public function pendingApp() {
        $page_data = null;
        $user_id = $this->session->user_id;
        $page_data['pendingApp'] = $this->user_model->pendingApp($user_id);
        $this->load->view('pending_approvals', $page_data);
        //echo '<pre>' ,print_r($page_data), '</pre>';
    }

    public function pendingPay() {
        isset($this->session->alert) ? $page_data['alert'] = $this->session->alert : $page_data['alert'] = '';
        $user_id = $this->session->user_id;
        $page_data['pendingPay'] = $this->user_model->pendingPay($user_id);
        //echo '<pre>' ,print_r($page_data), '</pre>';
        $this->load->view('pending_payments', $page_data);
    }

    public function pic_upload() {
        $config['upload_path'] = './assets/uploads/pics';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 1500;
        $config['max_width'] = 2400;
        $config['max_height'] = 1800;
        $config['encrypt_name'] = true;
        $config['file_ext_tolower'] = true;

        $page_dat['id'] = $this->input->post('phmd_id');
        $page_dat['user_id'] = $this->input->post('phu_id');
        $page_dat['category'] = $this->input->post('category');

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('pic')) {
            $page_dat['pop1'] = $this->upload->data('file_name');
            $this->user_model->add_pic($page_dat);
            $this->session->alert = '<div class="row">
					             <div class="alert alert-info fade in nomargin text-center">
										<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
										<p><h5>Proof of payment has been Uploaded!</h5></p>
                                    
									</div>
						      </div>';
            redirect('user/pendingPay');
        } else {

            $this->session->alert = '<div class="row">
					             <div class="alert alert-warning fade in nomargin text-center">
										<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
										<p><h5>Proof of payment Upload failed!</h5></p>
                                    
									</div>
						      </div>';
            redirect('user/pendingPay');
        }
    }

    public function history() {
        $page_data['pending_donation'] = $this->user_model->history($this->session->user_id);
        $this->load->view('history', $page_data);
        //var_dump($page_data);
    }

    public function referrals() {
        $this->load->view('referrals');
    }

    public function provideHelp($val = null) {
        $dat['alert'] = null;
        if (isset($_POST['submit']) && ($val === $this->session->token) && $this->token->validate()) {
            $data['category'] = $this->input->post('category');
            $data['amount'] = $this->input->post('amount');
            $data['user_id'] = $this->session->user_id;
            $check_openPH = $this->user_model->check_openPH($data);
            if ($check_openPH == false) {
                $dat['alert'] = "<div class='row'>
					        <div class='alert alert-danger fade in nomargin text-center'>
						    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
							<h4>Sorry You can't make another donation!</h4>
							<p>You have an unconfirmed transaction.</p>
							</div>
						    </div>";
                $this->load->view('provide_help', $dat);
            } else {
                $dat['alert'] = "<div class='row'>
					           <div class='alert alert-success fade in nomargin text-center'>
							   <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
							   <h4>Congratulations!</h4>
							   <p>Your donation has been successfully saved. </p>
							   </div>
						       </div>";
                $this->load->view('provide_help', $dat);
            }
        } else
            $this->load->view('provide_help', $dat);
    }

}
