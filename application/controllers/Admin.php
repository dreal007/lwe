<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{  
		if ($this->session->AdminIsLogged !== 'yes' )
	       $this->load->view('admin/login');
	  
	    else redirect('admin/dashboard', 'refresh'); 
		
	}

//-----------------------------------------Admin Login-----------------------------------------------------	
	
	 public function login() {
        $data['pass_error'] = '';
        $data['user_error'] = '';
        
        if (isset($_POST['username'])) {
            if ($this->form_validation->run('adminlogin') == TRUE) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $validated = $this->admin_model->login($username);

            }

            if (!empty($validated)) {
                foreach ($validated as $temp) {
                    $p_hash = $temp->password;
                    if (password_verify($password, $p_hash)) {
                        $this->session->AdminIsLogged = 'yes';
                        $this->session->user_id = $temp->id;
                        $this->session->role = ucfirst($temp->role);
                        $this->session->username = ucfirst($temp->username);
                        redirect('admin/dashboard', 'refresh');
                    } else {
                        $data['pass_error'] = '<strong> is incorrect!</strong><br>';
                        $this->load->view('admin/login', $data);
                    }
                }
            } else {
                $data['user_error'] = '<strong> is not registered !</strong><br>';
                $this->load->view('admin/login', $data);
            }
        } else
            $this->load->view('admin/login', $data);
    }


 //-----------------------------------------Admin Dashboard-----------------------------------------------------   
	
	public function dashboard($page_data = []){
	   if ($this->session->AdminIsLogged !== 'yes' || $this->session->role == 'none')
	       redirect('admin/login', 'refresh');
	  
	    else{
		$page_data['users'] = $this->admin_model->count_users();
		$this->load->view('admin/dashboard', $page_data);
        }		
	}
	
//-----------------------------------------Admin view users--------------------------------------------------------		
    public function viewUsers(){
	   $data['users']= $this->admin_model->view_users();
	   $this->load->view('admin/users', $data);
	}
	
	
	public function completedDon(){}
	
	public function unmerged(){}
	
	public function mergedUnconf(){}

//-----------------------------------------Admin Logout--------------------------------------------------------	
	
	public function logout(){
	   $this->session->sess_destroy();
        //delete_cookie('page');
        redirect(base_url() . 'admin/login', 'refresh');
	}
	
}
