<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

public function __construct() {
    parent::__construct();
    /* cache control */
    $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
    $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    $this->output->set_header('Pragma: no-cache');
    $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");
    $this->load->library('Recaptcha');
    $this->load->library('Token');
    $this->load->helper('download');
    date_default_timezone_set("Africa/Lagos");
}

public function index(){
    $this->load->view('index');
      //$this->load->view('reception');
}

  public function reception(){
      $this->checkToken('reception');
  }

  public function exhibition_hall(){
      $this->checkToken('exhibitionhall');
  }


 public function church_growth(){
      $this->checkToken('cgi');
 }

 public function nss_zone_one(){
      $this->checkToken('nssz1');
 }

  public function seminar_hub(){
      $this->checkToken('seminarhub');
  }

  public function exhibition_map(){
      $this->checkToken('exhibitionmap');
  }

  public function campus_a(){
      $this->checkToken('campusa');
  }

  public function campus_b(){
      $this->checkToken('campusb');
  }

  public function campus_c(){
      $this->checkToken('campusc');
  }

  public function innercity(){
      $this->checkToken('innercity');
  }

  public function healing_school(){
    $this->checkToken('healing_sch');
  }

  public function loveworld_publishing(){
    $this->checkToken('loveworld_pub');
  }

  public function internet_multimedia(){
    $this->checkToken('internet_multimedia');
  }

  public function campus_ministry(){
    $this->checkToken('campus_ministry');
  }

  public function loveworld_tm(){
    $this->checkToken('loveworld_tm');
  }

  public function loveworld_sat(){
    $this->checkToken('loveworld_sat');
  }

  public function loveworld_plus(){
    $this->checkToken('loveworld_plus');
  }

  public function loveworld_music(){
    $this->checkToken('loveworld_music');
  }

  public function loveworld_radio(){
    $this->checkToken('loveworld_radio');
  }

  public function haven(){
    $this->checkToken('haven');
  }

  public function pcdl(){
    $this->checkToken('pcdl');
  }

  public function gylf(){
    $this->checkToken('gylf');
  }

  public function rhapsody(){
    $this->checkToken('rhapsody');
  }

   public function rhapsody_bible(){
    $this->checkToken('rhapsody_bible');
  }

  public function falf(){
    $this->checkToken('falf');
  }

  public function ism(){
    $this->checkToken('ism');
  }

  public function teens_ministry(){
    $this->checkToken('teens_ministry');
  }

  public function childrens_ministry(){
    $this->checkToken('childrens_ministry');
  }

  public function cofi(){
    $this->checkToken('cofi');
  }

  public function loveworld_networks(){
    $this->checkToken('loveworld_networks');
  }

  public function reachout_campaigns(){
    $this->checkToken('reachout_campaigns');
  }

  public function medical_missions(){
    $this->checkToken('medical_missions');
  }

  // public function download($filename){
  //   $pth = file_get_contents(base_url('assets/files/').$filename.'.pdf');
  //   force_download($filename.'.pdf', $pth);
  // }

  public function download($filename){
    $pth = file_get_contents(base_url('assets/files/').$filename);
    force_download($filename, $pth);
  }

  public function reg(){
      if($this->input->post('user')){
          $user = $this->input->post('user');
           $data['fullname'] = $user['fullname'];
           $data['email'] = $user['email'];
           $data['phone'] = $user['phone'];
           $data['gender'] = $user['gender'];
           $data['church'] = $user['church'];
           $data['country'] = $user['country'];
           $data['designation'] = $user['designation'];
           $data['zone'] = $user['zone'];
           $data['region'] = $user['region'];
           $data['birthday'] = $user['birthday'];
           $data['access_token'] = md5($data['email'].$data['phone']);

           $resp = $this->user_model->register($data);
           if($resp['status'] === true){
               $resp_data['status']  = true;
               $resp_data['access_token'] = $data['access_token'];
               $resp_data['name'] = $data['fullname'];
               $resp_data['message'] = 'Registered successfully';
               echo json_encode($resp_data);
           }
           elseif($resp['error']) {
              echo json_encode($resp);
           }
      }
      else{
          echo json_encode('no data');
      }
  }

  public function checkToken($page = ''){
      // $token = base64_decode($this->input->get('q'));
      // $result = $this->user_model->verifyToken($token);
      //  if($result['data']['0']->access_token === $token){
      //      $this->load->view($page);
      //  }
      //  else $this->load->view('index');

      if(!$page == '') $this->load->view($page);

      else $this->load->view('reception');
  }

  public function enter(){
      if($this->input->post('phone')){
          $enterResp = $this->user_model->enter($this->input->post('phone'));
          if($enterResp['data']){
              $enterResp['status'] = true;
              echo json_encode($enterResp);
          }
          elseif($enterResp['error']){
              $enterResp['status'] = false;
              echo json_encode($enterResp);
          }
      }
      else{
          echo json_encode('no phone number');
      }
  }

  public function signup(){
      if($this->input->post('signup')){
          $checkSignup = $this->user_model->check_signup($this->input->post('signup'));
          if($checkSignup['data']){
              $checkSignup['status'] = true;
              echo json_encode($checkSignup);
          }
          else{
              $signupResp = $this->user_model->signup($this->input->post('signup'));
              if($signupResp['status'] === true){
                  echo json_encode($signupResp);
              }
              elseif($signupResp['error']){
                  $signupResp['status'] = false;
                  echo json_encode($signupResp);
              }
          }
      }
      else {
        echo json_encode('No signup Info');
      }
  }

//  ----------------------------------------------Account Activate-----------------------------
    public function activate($email, $token){
        $data['email'] =$email;
        $data['token'] =$token;
        $activated = $this->user_model->activate($data);
        if($activated){
          $this->load->view('act_success');
        }
    }


// ----------------------------------------------Logout-----------------------------------------

	public function logout(){
	   $this->session->sess_destroy();
        //delete_cookie('page');
        redirect(base_url() . 'home/login', 'refresh');
	}

}
