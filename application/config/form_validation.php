<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
    'signup' => array(
        array(
            'field' => 'fname',
            'label' => 'First name',
            'rules' => 'trim|required|min_length[3]'
        ),
		 array(
            'field' => 'lname',
            'label' => 'Last name',
            'rules' => 'trim|required|min_length[3]'
        ),
		
		 array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|required|min_length[3]'
        ),
        
        array(
            'field' => 'email',
            'label' => 'Email Address',
            'rules' => 'trim|required|valid_email'
        ),
		
		 array(
            'field' => 'phone',
            'label' => 'Phone number',
            'rules' => 'trim|required|min_length[10]|numeric'
        ),
		
		 array(
            'field' => 'referrer',
            'label' => 'Referrer',
            'rules' => 'trim'
        ),
		
		 array(
            'field' => 'state',
            'label' => 'State',
            'rules' => 'trim|required|min_length[3]'
        ), 
		
		array(
            'field' => 'country',
            'label' => 'Country',
            'rules' => 'trim|required|min_length[3]'
        ),
		
		 array(
            'field' => 'bankname',
            'label' => 'Bank name',
            'rules' => 'trim|required|min_length[3]'
        ),
		
		 array(
            'field' => 'accountname',
            'label' => 'Account name',
            'rules' => 'trim|required|min_length[3]'
        ),
		
		 array(
            'field' => 'acctype',
            'label' => 'Account type',
            'rules' => 'trim|required|min_length[3]'
        ),
		
		 array(
            'field' => 'accnum',
            'label' => 'Account number',
            'rules' => 'trim|required|min_length[10]|numeric'
        ),
        
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|min_length[5]'
        ),
        
         array(
            'field' => 'passwordconf',
            'label' => 'Password confirmation',
            'rules' => 'required|matches[password]'
        ),
        
         array(
            'field' => 'agreeterms',
            'label' => 'Agree to Terms',
            'rules' => 'required'
        )
        
    ),
    
    'login' => array(
        
         array(
            'field' => 'email',
            'label' => 'Email Address',
            'rules' => 'trim|required|valid_email'
        ),
        
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required'
        )
        
    ),


    'adminlogin' => array(
        
         array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim|required'
        ),
        
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required'
        )
        
    )
    
    
);


