<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function register($credentials = []) {
        $resp['status'] = $this->db->insert('users', $credentials);
        $resp['error'] = $this->db->error();
        return $resp;
    }

    public function enter($phone){
        $query = $this->db->get_where('users', array('phone' => $phone), 1);
        $row['data'] = $query->result();
        $row['error'] = $this->db->error();
        return $row;
    }

    public function check_signup($val =[]){
        $query = $this->db->get_where('signups', array('ministry_arm' => $val['ministry_arm'], 'user_id' => $val['user_id']), 1);
        $row['data'] = $query->result();
        $row['error'] = $this->db->error();
        return $row;
    }

    public function signup($credentials = []) {
        $resp['status'] = $this->db->insert('signups', $credentials);
        $resp['error'] = $this->db->error();
        return $resp;
    }

    public function verifyToken($token){
        $query = $this->db->get_where('users', array('access_token' => $token), 1);
        $row['data'] = $query->result();
        $row['error'] = $this->db->error();
        return $row;
    }

    public function login($email = '') {
        $query = $this->db->get_where('users', array('email' => $email, 'activated' => '1'), 1);
        $row = $query->result();
        return $row;
    }

    public function activate($data = array()) {
        $query = $this->db->get_where('users', array('email' => $data['email'], 'token' => $data['token']));
        if ($query->num_rows() > 0) {
            $query = $this->db->update('users', array('activated' => '1'), array('email' => $data['email'], 'token' => $data['token']));
            if ($query) {
                return true;
            } else
                return false;
        }
    }

}
