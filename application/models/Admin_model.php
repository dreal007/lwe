<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function add_pic($data) {
        $query = $this->db->insert('files',  $data);
        if ($query) {
            return true;
        }
    }
    
    public function view_users(){
        $query = $this->db->query("SELECT * FROM users");
        return $query->result();
    }

     public function login($username = '') {
        $query = $this->db->get_where('admin', array('username' => $username, 'role' => 'admin'), 1);
        $row = $query->result();
        return $row;
    }

    public function count_users() {
        $this->db->where('activated', '1');
        $this->db->from('users');
        return $this->db->count_all_results();
   }

    public function insert_gdonation($data = array()){
	    
	}
	public function check_openPH($data = array()){
	   $query = $this->db->get_where('mdonation', array('user_id' => $data['user_id'], 'category' => $data['category'], 'confirmed' => '0'));
       if ($query->num_rows() > 0){
	       return false;
	   }
	   return $this->db->insert('mdonation', $data);
	}
	
	public function pendingPay($id){
	   $query = $this->db->get_where('mdonation', array('user_id' => $id, 'confirmed' => '0'));
       if ($query->num_rows() > 0){
	      return $query->result();
	   }
	}
	
	
	
	
}	
