<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_model extends CI_Model {

 public function insert_gd(){

 $query = $this->db->query("SELECT id AS md_id, user_id, category
            FROM mdonation 
            WHERE merged = '1'  AND confirmed = '1' AND status = 'pending'
			ORDER BY created ASC
			LIMIT 1 ;");
			$query_res = $query->result();
		if($query_res){	
  			$this->db->insert_batch('gdonation', $query_res);
			$this->db->insert_batch('gdonation', $query_res);
		 }	
 $query = $this->db->query("SELECT mdonation.id
            FROM mdonation 
			RIGHT JOIN gdonation ON mdonation.id = gdonation.md_id
            WHERE mdonation.merged = '1'  AND mdonation.confirmed = '1' AND mdonation.status = 'pending'
			ORDER BY mdonation.created ASC
			LIMIT 1 ;");
			$query_res = $query->result();
			
			foreach ($query_res as $val){
			  $id = $val->id;
			  $this->db->query("UPDATE mdonation SET status = 'approved' WHERE id = '$id';");
			}
}

     public function merge(){
	    $query_first = $this->db->query("SELECT id AS phmd_id, user_id, category
            FROM mdonation 
            WHERE merged = '0'  AND confirmed = '0' AND status = 'pending'
			ORDER BY created ASC
			LIMIT 200 ;");
			$query_res_first = $query_first->result();			
		
		foreach($query_res_first as $res){
				$phmd_id = $res->phmd_id;
				$phu_id = $res->user_id;
				$category = $res->category;	
				$update = $this->db->query("UPDATE gdonation 
				                  SET phu_id = '$phu_id', merged = '1', phmd_id = '$phmd_id'
								  WHERE category = '$category' AND merged = '0'
								  AND confirmed = '0' AND status = 'pending' AND user_id != '$phu_id'
								  ORDER BY time ASC
								  LIMIT 1;");
								  
		        if($update){
				    $select = $this->db->query("SELECT id, md_id, phu_id, category FROM gdonation
                                                WHERE phmd_id = '$phmd_id' AND phu_id = '$phu_id'
                                                AND category = '$category' AND merged = '1'						
											    LIMIT 1;");
					$select = $select->row();
					if(isset($select)){
                     $md_id1   = $select->md_id;
					 $phu_id1  = $select->phu_id;
					 $category = $select->category;
					 $date = new DateTime('now');
					 $date = $date->format('Y-m-d H:i:s');
				     $update2 = $this->db->query("UPDATE mdonation 
				                 SET merged = '1', time_left = '$date'
								 WHERE user_id = '$phu_id1' AND category = '$category' AND merged = '0'
								 AND confirmed = '0' AND status = 'pending'
								 LIMIT 1;");
				  	}
				
				}                   
		}
			
	 }
	 
	public function suspend(){
	   $select = $this->db->query("SELECT id, user_id FROM mdonation WHERE merged = '1' AND confirmed = '0'
                                  AND status = 'pending' AND time_left + INTERVAL 24 HOUR <= CURRENT_TIMESTAMP 
								  LIMIT 20;");
		 $select = $select->result();
		 //echo '<pre>' ,print_r($select), '</pre>';
		 foreach($select as $val){
		  $id = $val->id;
		  $user_id = $val->user_id;
		   $update = $this->db->query("UPDATE mdonation SET status = 'suspended' 
		                              WHERE id = '$id' AND user_id = '$user_id'
									  LIMIT 1  ;");
									  
		  $update = $this->db->query("UPDATE gdonation SET merged = '0', phmd_id = '' ,phu_id = '' 
		                              WHERE phmd_id = '$id' AND phu_id = '$user_id'
									  LIMIT 1  ;");
									  
          $update = $this->db->query("UPDATE users SET activated = '0' 
		                              WHERE id = '$user_id'
									  LIMIT 1  ;"); 									  
								      
		}
								  						 
	} 

}

