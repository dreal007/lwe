<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
    
     
     public function reg_mail($data = []) {
        $this->load->library('email');
        $subject = 'CoinCup Account Activation';
        $message = '<p>Click Here to <a href='.base_url('home/activate/').$data['email'].'/'.$data['token'];'>activate</a> your account.</p>';
        // Get full html:
        $body = '<!DOCTYPE html">
             <html>
              <head>
			  <meta charset="UTF-8">
		      <meta name="keywords" content="CoinCup | Registration success" />
		      <meta name="description" content="CoinCup | Registration success">
		      <meta name="author" content="CoinCup | Registration success">
              <!--<meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />-->
			  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
              <title>' . html_escape($subject) . '</title>
		      <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
              <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
              </head>
              <body>
			  	<div class="row">
		          <div class="col-lg-8 col-lg-offset-2 alert alert-primary fade in text-center">
                    <h4><strong>Thank You!</strong> Your registration on CoinCup was Successful.</h4>
                  </div>
	            </div><br>
              ' . $message . '
              </body>
              </html>';
               $result = $this->email
                              //->from('drealslimguy007@gmail.com')
							   ->from('account@coincup.net')
                //->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
                              ->to($data['email'])
                              ->subject($subject)
                              ->message($body)
                            //->attach($file)
                              ->send();
    }
    
    
     public function report_mail($data = []) {
        $this->load->library('email');
        $subject = 'This is a test';
        $message = '<p>This message has been sent for testing purposes.</p>';
        // Get full html:
        $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
             <html xmlns="http://www.w3.org/1999/xhtml">
              <head>
              <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
              <title>' . html_escape($subject) . '</title>
              <style type="text/css">
                body {
                font-family: Arial, Verdana, Helvetica, sans-serif;
                font-size: 16px;
                }
              </style>
              </head>
              <body>
              ' . $message . '
			  <sript src="https://code.jquery.com/jquery-1.12.4.js"></script>
              </body>
              </html>';
// Also, for getting full html you may use the following internal method:
//$body = $this->email->full_html($subject, $message);
        $file = $data['file_name'];
        $result = $this->email
                ->from('drealslimguy007@gmail.com')
                //->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
                ->to('chuksthewoundy@yahoo.com')
                ->subject($subject)
                ->message($body)
                ->attach($file)
                ->send();
    }
}

